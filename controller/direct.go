package controller

import (
	"brifice/database"
	"brifice/structs"
	"log"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

func DirectInvestOptionDetail(c *gin.Context) {
	id_produk, err := strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil {
		c.JSON(200, gin.H{"status": "error", "detail": "conrter error"})
		return
	}

	id, err2 := getId(c)
	if err2 != nil {
		c.JSON(200, gin.H{"status": "error", "detail": "get ID ERROR"})
		return
	}
	jumlah_investasi := 1250000
	p := InvOptDetail(id, id_produk, float64(jumlah_investasi))
	c.JSON(200, gin.H{"status": "success", "data": p["produk"]})
	return
}

func DirectInvestOption(c *gin.Context) {
	tokenString := c.Request.Header.Get("Authorization")
	tokenString = strings.Split(tokenString, "Bearer ")[1]
	detail := strings.Split(UserDetail(tokenString), ",")
	id, err := strconv.ParseUint(detail[2], 10, 32)
	if err != nil {
		log.Fatal("parsing id gagal")
		return
	}
	var nas database.Nasabah
	var r database.Profil_Resiko
	var p structs.Produk_Investasi
	p_list := []structs.Produk_Investasi{}

	q := database.DB.Select("profil_resiko_id").Find(&nas, id)
	if q.Error != nil {
		log.Fatal("error profil nasabah: ", q.Error.Error())
		return
	}
	q = database.DB.Find(&r, nas.Profil_Resiko_Id)
	q = database.DB.Preload("Produk_investasi").Find(&r)
	for i := 0; i < len(r.Produk_investasi); i++ {
		p.Id = r.Produk_investasi[i].Id
		p.Nama_produk_investasi = r.Produk_investasi[i].Nama_produk_investasi
		p.Jenis_produk = r.Produk_investasi[i].Jenis_produk
		p.Deskripsi_produk = r.Produk_investasi[i].Deskripsi_produk
		p_list = append(p_list, p)
	}
	if q.Error != nil {
		log.Fatal("error produk: ", q.Error.Error())
		return
	}
	c.JSON(200, gin.H{"status": "success", "detail": &p_list})
	return
}
