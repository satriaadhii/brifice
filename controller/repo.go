package controller

import (
	"brifice/database"
	"brifice/structs"
	"fmt"
	"log"
	"math"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func MapToSlice(a map[string]structs.Transaksi) []structs.Transaksi {
	var tr []structs.Transaksi
	for _, t := range a {
		tr = append(tr, t)
	}
	return tr
}
func Analyzing(tk []structs.Analyze_expense, tm []structs.Analyze_income, id uint64) gin.H {
	var tkfin, tmfin []structs.FinRes
	var a, b structs.FinRes
	var avg_tk, avg_tm, per_tk, per_tm float64
	for _, r := range tk {
		avg_tk = avg_tk + float64(r.Nominal)
	}
	for _, r := range tm {
		avg_tm = avg_tm + float64(r.Nominal)
	}

	for _, r := range tk {
		per_tk = ((float64(r.Nominal) / (avg_tk)) * 100)
		a.Nominal = r.Nominal
		a.Percent = float32(per_tk)
		a.Remarks = r.Remarks
		tkfin = append(tkfin, a)
	}

	for _, r := range tm {
		per_tm = ((float64(r.Nominal) / (avg_tm)) * 100)
		b.Nominal = r.Nominal
		b.Remarks = r.Remarks
		b.Percent = float32(per_tm)
		tmfin = append(tmfin, b)
	}

	avg_tk = (avg_tk / 3)
	avg_tm = (avg_tm / 3)
	idle_money := avg_tm - avg_tk
	var saldo_akhir float64
	saldo_akhir = 0

	if math.Signbit(float64(idle_money)) == true {
		var bank_profile []database.Bank_Profile

		database.DB.Where("id_nasabah = ?", id).Select("saldo").Find(&bank_profile)
		for _, b2 := range bank_profile {
			saldo_akhir = saldo_akhir + b2.Saldo
			//log.Println("index - ", i, "saldo akhir: ", saldo_akhir)
		}
		idle_money = (float64(saldo_akhir) + avg_tm) - avg_tk
	}
	x := gin.H{"status": "success", "detail": gin.H{"keluar": tkfin, "masuk": tmfin}, "avg_pengeluaran": avg_tk, "avg_pemasukkan": avg_tm, "idle_money": idle_money}
	return x
}

func PreAnalyze(tk, tm []structs.Result, id uint64) gin.H {
	log.Println("total transaksi keluar: ", len(tk))
	var tkfin, tmfin []structs.FinalResult
	var a, b structs.FinalResult
	var avg_tk, avg_tm, per_tk, per_tm float32
	for _, r := range tk {
		avg_tk = avg_tk + r.Nominal
	}
	for _, r := range tm {
		avg_tm = avg_tm + r.Nominal
	}

	for _, r := range tk {
		per_tk = ((r.Nominal / (avg_tk)) * 100)
		a.Id = r.Id
		//a.Nominal = r.Nominal
		a.Percent = per_tk
		a.Remarks = r.Remarks
		a.Trx_code = r.Trx_code
		tkfin = append(tkfin, a)
	}

	for _, r := range tm {
		per_tm = ((r.Nominal / (avg_tm)) * 100)
		b.Id = r.Id
		//b.Nominal = r.Nominal
		b.Remarks = r.Remarks
		b.Percent = per_tm
		b.Trx_code = r.Trx_code
		tmfin = append(tmfin, b)
	}

	x := gin.H{"status": "success", "detail": gin.H{"keluar": tkfin, "masuk": tmfin}, "avg_pengeluaran": avg_tk, "avg_pemasukkan": avg_tm, "idle_money": avg_tm - avg_tk}
	return x
}

func ErrorResp(detail string) gin.H {
	return gin.H{"status": "error", "detail": detail}
}

func SuccessRespData(detail gin.H) gin.H {
	return gin.H{"status": "success", "detail": detail}
}

func SucResp(c *gin.Context, http_status int, data interface{}, meta interface{}) {
	c.JSON(http_status, gin.H{"status": "success", "data": data, "meta": meta})
}

func Mutasi(id_nasabah uint) ([]structs.Result, []structs.Result) {
	three_month := time.Now().AddDate(0, -3, 0)
	tm, tk := []structs.Result{}, []structs.Result{}
	n := []database.Nasabah{}
	database.DB.Model(&n).Where("transaksi_keluar.transaction_timestamp > ? AND nasabah.id = ?AND transaksi_keluar.status = true", three_month, id_nasabah).Select("transaksi_keluar.id, transaksi_keluar.remarks, transaksi_keluar.trx_code, transaksi_keluar.nominal_debet as Nominal").Joins("JOIN bank_profile on bank_profile.id_nasabah = nasabah.id").Joins("JOIN transaksi_keluar on transaksi_keluar.id_bank = bank_profile.id").Find(&tk)
	database.DB.Model(&n).Where("transaksi_masuk.transaction_timestamp > ? AND nasabah.id = ?AND transaksi_masuk.status = true", three_month, id_nasabah).Select("transaksi_masuk.id, transaksi_masuk.remarks, transaksi_masuk.trx_code, transaksi_masuk.nominal_kredit as Nominal").Joins("JOIN bank_profile on bank_profile.id_nasabah = nasabah.id").Joins("JOIN transaksi_masuk on transaksi_masuk.id_bank = bank_profile.id").Find(&tm)
	return tk, tm
}

func CountInvestDPLK(idle_money float64, tanggal_lahir time.Time) float64 {
	im := idle_money
	tanggal_pensiun := tanggal_lahir.AddDate(0, 0, 20075)
	kepesertaan := tanggal_pensiun.Sub(time.Now())
	kepesertaan = time.Duration(kepesertaan.Hours() / 730)
	interest := (0.08 - 0.007) / 12
	future_value := (1 + interest)
	future_value = math.Pow(future_value, float64(kepesertaan))
	future_value = future_value - 1
	future_value = im * future_value / interest

	fmt.Println("idle money: ", im /* "\ntanggal lahir: ", tanggal_lahir, "\ntanggal pensiun: ", tanggal_pensiun, */, "\nlama kepesertaan: ", kepesertaan, "\ninterest: ", interest, "\nfuture value: ", future_value)
	return future_value
}

func CountInvestNew(nama_produk string, idle_money float64, rate_bunga float64, tanggal_lahir time.Time) (uint, uint, uint) {
	im := idle_money
	fmt.Println(nama_produk, idle_money, rate_bunga, tanggal_lahir)
	total1, total2, total3 := 0.0, 0.0, 0.0
	switch nama_produk {
	case "Deposito":
		total1 = CountInvest(im, rate_bunga, 6)
		total2 = CountInvest(im, rate_bunga, 12)
		total3 = CountInvest(im, rate_bunga, 24)
	case "DPLK":
		total1 = CountInvestDPLK(im, tanggal_lahir)
		total2 = 0
		total3 = 0
	case "Britama Rencana":
		total1 = CountInvest(im, rate_bunga, 12)
		total2 = CountInvest(im, rate_bunga, 60)
		total3 = CountInvest(im, rate_bunga, 120)
	case "Reksadana":
		total1 = CountInvest(im, rate_bunga, 6)
		total2 = CountInvest(im, rate_bunga, 12)
		total3 = CountInvest(im, rate_bunga, 24)
	default:
		log.Println("Produk Tidak Tersedia")
	}
	math.Ceil(total1)
	math.Ceil(total2)
	math.Ceil(total3)
	return uint(total1), uint(total2), uint(total3)
}

func CountInvest(idle_money float64, rate_bunga float64, bulan int) float64 {
	im := idle_money
	rate_bunga = (rate_bunga / 100)
	bunga := (im * rate_bunga * 30) / 365
	pajak := (float64(20) / 100) * bunga
	manfaat := bunga - float64(pajak)
	for i := 2; i <= bulan; i++ {
		im = (im + manfaat) + idle_money
		bunga = (rate_bunga * float64(im) * 30) / 365
		pajak = (float64(20) / 100) * bunga
		manfaat = bunga - float64(pajak)
	}
	return im
}

func InvOpt(id uint64, idle_money float64) gin.H {
	var nas database.Nasabah
	q := database.DB.Select("profil_resiko_id, tanggal_lahir").Find(&nas, id)
	if q.Error != nil {
		log.Fatal("error profil nasabah: ", q.Error.Error())
	}
	var r database.Profil_Resiko
	err := database.DB.Find(&r, nas.Profil_Resiko_Id)
	if err.RowsAffected == 0 {
		return gin.H{"status": "failed", "detail": "risk_id_notfound"}
	}
	database.DB.Debug().Preload("Produk_investasi", func(db *gorm.DB) *gorm.DB {
		return db.Order("produk_investasi.nama_produk_investasi")
	}).Find(&r)
	var Aio structs.AnalyzeInvOpt
	Aio_list := []structs.AnalyzeInvOpt{}

	for i := 0; i < len(r.Produk_investasi); i++ {
		Aio.Id = r.Produk_investasi[i].Id
		Aio.Nama_produk_investasi = r.Produk_investasi[i].Nama_produk_investasi
		Aio.Jenis_produk = r.Produk_investasi[i].Jenis_produk
		Aio.Deskripsi_produk = r.Produk_investasi[i].Deskripsi_produk
		Aio.Idle_money = uint(math.Ceil(idle_money))
		Aio.Total_1, Aio.Total_2, Aio.Total_3 = CountInvestNew(Aio.Nama_produk_investasi, idle_money, r.Produk_investasi[i].Bunga, nas.Tanggal_lahir)
		Aio_list = append(Aio_list, Aio)
	}
	return gin.H{"status": "success", "detail": &Aio_list}
}

func InvOptDetail(id uint64, id_produk uint64, idle_money float64) gin.H {
	var p database.Produk_Investasi
	var Aio structs.AnalyzeInvOpt
	var nas database.Nasabah

	q := database.DB.Select("id", "nama_produk_investasi", "jenis_produk", "deskripsi_produk", "bunga").First(&p, &id_produk)
	if q.Error != nil {
		log.Println("error fetching", q.Error.Error())
	}
	q = database.DB.Select("profil_resiko_id, tanggal_lahir").Find(&nas, id)
	if q.Error != nil {
		log.Println("error fetching user detail", q.Error.Error())
	}
	log.Println(nas.Tanggal_lahir)
	Aio.Id = p.Id
	Aio.Nama_produk_investasi = p.Nama_produk_investasi
	Aio.Jenis_produk = p.Jenis_produk
	Aio.Deskripsi_produk = p.Deskripsi_produk
	Aio.Idle_money = uint(math.Ceil(idle_money))
	Aio.Total_1, Aio.Total_2, Aio.Total_3 = CountInvestNew(Aio.Nama_produk_investasi, idle_money, p.Bunga, nas.Tanggal_lahir)
	return gin.H{"produk": &Aio}
}
