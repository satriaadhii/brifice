package database

import "gorm.io/gorm"

func ProdukOrder(db *gorm.DB, ord string) *gorm.DB {
	return db.Order(ord)
}
