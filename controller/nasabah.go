package controller

import (
	"brifice/database"
	"brifice/structs"
	"brifice/utils"
	"math"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

type Nasabah struct{}
type NasabahList struct {
	CreatedAt        time.Time         `json:"register"`
	ID               uint              `json:"id"`
	Nama             string            `json:"nama"`
	Username         string            `json:"username"`
	DaftarPengajuans []DaftarPengajuan `json:"daftar_pengajuan" gorm:"foreignKey:Id_nasabah;references:ID"`
}

type DaftarPengajuan struct {
	Id_nasabah      uint
	JumlahPengajuan uint `json:"jumlah_pengajuan"`
	PengajuanStatus int  `json:"status_pengajuan"`
}

type NasabahRes struct {
	CreatedAt       time.Time `json:"register"`
	ID              uint      `json:"id"`
	Nama            string    `json:"nama"`
	Username        string    `json:"username"`
	JumlahInvestasi uint      `json:"jumlah_investasi"`
}

type Portofolio struct {
	Pengajuan  time.Time `json:"pengajuan"`
	Nama       string    `json:"nama_produk_investasi"`
	Jumlah     uint      `json:"jumlah_produk_investasi"`
	Pendapatan float64   `json:"pendapatan"`
}

type Total struct {
	Investasi  uint    `json:"total_investasi"`
	Pendapatan float64 `json:"pendapatan"`
}

func (NasabahList) TableName() string     { return "nasabah" }
func (DaftarPengajuan) TableName() string { return "daftar_pengajuan" }

type NoRekening struct {
	NoRekening string
}
type NasabahDetail struct {
	Nama         string    `json:"nama"`
	TanggalLahir time.Time `json:"tanggal_lahir"`
	Email        string    `json:"email"`
	NoRekening   []string  `json:"nomor_rekening"`
	Alamat       string    `json:"alamat"`
	NoPonsel     string    `json:"no_hp"`
	CreatedAt    time.Time `json:"register"`
}

// Not Cleared yet!
func (p *Nasabah) GetAll(c *gin.Context) {
	var n []NasabahList
	var res []NasabahRes
	var r NasabahRes
	pagination := utils.Paginate(c)
	totalData := database.DB.Preload("DaftarPengajuans").Find(&n).RowsAffected
	queryBuilder := database.DB.Limit(pagination.Limit).Offset((pagination.Page - 1) * pagination.Limit)
	err := queryBuilder.Preload("DaftarPengajuans").Find(&n)
	if err.Error != nil {
		c.JSON(200, gin.H{"status": "failed", "detail": err.Error.Error()})
		return
	}
	//fmt.Println(&n)
	for _, nl := range n {
		r.CreatedAt = nl.CreatedAt
		r.ID = nl.ID
		r.Nama = nl.Nama
		r.Username = nl.Username
		for _, dp := range nl.DaftarPengajuans {
			if dp.PengajuanStatus == 3 {
				r.JumlahInvestasi = r.JumlahInvestasi + dp.JumlahPengajuan
			}
		}
		res = append(res, r)
		r.JumlahInvestasi = 0
	}
	SucResp(c, http.StatusOK, &res, gin.H{"page": pagination.Page, "total_data": totalData, "total_page": math.Ceil(float64(totalData) / float64(pagination.Limit))})
	return
}

func (p *Nasabah) GetDetail(c *gin.Context) {
	var na database.Nasabah
	var n NasabahDetail
	var b []database.Bank_Profile
	id := c.Param("id")
	err := database.DB.Debug().Model(&database.Nasabah{}).Select("nasabah.nama, nasabah.tanggal_lahir, nasabah.email, nasabah.alamat, nasabah.no_ponsel, nasabah.created_at").Where("nasabah.id = ?", id).Find(&na)
	//err := database.DB.Debug().Preload("BankProfiles").Find(&n, id)
	if err.Error != nil {
		c.JSON(200, gin.H{"status": "failed", "detail": err.Error.Error()})
		return
	}
	if err.RowsAffected == 0 {
		c.JSON(200, gin.H{"status": "failed", "detail": "record not found"})
		return
	}
	n.Nama = na.Nama
	n.TanggalLahir = na.Tanggal_lahir
	n.Email = na.Email
	n.Alamat = na.Alamat
	n.NoPonsel = na.No_ponsel
	n.CreatedAt = na.CreatedAt
	err = database.DB.Debug().Where("id_nasabah = ?", id).Select("no_rekening").Find(&b)
	if err.Error != nil {
		c.JSON(200, gin.H{"status": "failed", "detail": err.Error.Error()})
		return
	}
	if err.RowsAffected == 0 {
		SucResp(c, http.StatusOK, &n, gin.H{"status": "success"})
		return
	} else {
		for _, b2 := range b {
			n.NoRekening = append(n.NoRekening, b2.No_rekening)
		}
		SucResp(c, http.StatusOK, &n, gin.H{"status": "success"})
		return
	}
}

func (p *Nasabah) GetAnalisis(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	var ak []structs.Analyze_expense
	var am []structs.Analyze_income
	database.DB.Where("id_nasabah = ? AND deleted_at is NULL", id).Find(&ak)
	database.DB.Where("id_nasabah = ? AND deleted_at is NULL", id).Find(&am)
	res := Analyzing(ak, am, uint64(id))
	c.JSON(200, &res)
	return
}

func (p *Nasabah) GetPortofolio(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	var t Total
	var dp []Portofolio
	var Portolist []Portofolio
	var Porto Portofolio
	err := database.DB.Debug().Model(&database.Daftar_Pengajuan{}).Where("daftar_pengajuan.pengajuan_status = ? AND daftar_pengajuan.id_nasabah = ?", 3, id).Select("daftar_pengajuan.created_at as pengajuan, pi.nama_produk_investasi as nama, daftar_pengajuan.jumlah_pengajuan as jumlah").Joins("left join produk_investasi pi on pi.id = daftar_pengajuan.id_produk_investasi").Scan(&dp).Error
	if err != nil {
		c.JSON(200, gin.H{"status": "failed", "detail": err.Error})
		return
	}

	for _, d := range dp {
		Porto.Jumlah = 0
		Porto.Pendapatan = 0

		Porto.Nama = d.Nama
		Porto.Jumlah = d.Jumlah
		Porto.Pengajuan = d.Pengajuan
		Porto.Pendapatan = Porto.Pendapatan + (0.1 * float64(d.Jumlah))
		Portolist = append(Portolist, Porto)
		t.Investasi = t.Investasi + Porto.Jumlah
		t.Pendapatan = t.Pendapatan + Porto.Pendapatan
	}
	SucResp(c, http.StatusOK, gin.H{"list": &Portolist, "total_investasi": &t.Investasi, "total_pendapatan": &t.Pendapatan}, gin.H{"status": "success"})
	return
}
