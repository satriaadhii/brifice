package utils

import (
	"strconv"

	"github.com/gin-gonic/gin"
)

type Pagination struct {
	Limit int    `json:"limit"`
	Page  int    `json:"page"`
	Sort  string `json:"sort"`
}

func Paginate(c *gin.Context) Pagination {
	limit := 2
	page := 1
	sort := "created_at asc"
	query := c.Request.URL.Query()
	for key, value := range query {
		qval := value[len(value)-1]
		switch key {
		case "limit":
			limit, _ = strconv.Atoi(qval)
			break
		case "page":
			page, _ = strconv.Atoi(qval)
			break
		case "sort":
			sort = qval
			break
		}
	}
	return Pagination{
		Limit: limit,
		Page:  page,
		Sort:  sort,
	}
}
