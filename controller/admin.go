package controller

import (
	"brifice/database"
	"brifice/structs"
	"log"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

func AdminLogin(c *gin.Context) {
	var n structs.Auth
	c.ShouldBind(&n)
	t := LoginHandler(n.Username, n.Password, "admin")
	if t.Status == "error" {
		c.JSON(401, gin.H{"status": t.Status, "detail": t.Data})
		return
	}
	dt := strings.Split(t.Data, ",")
	id, _ := strconv.Atoi(dt[1])
	var res = gin.H{
		"status": "success",
		"data": gin.H{
			"id":       id,
			"username": dt[2],
			"role":     dt[3],
			"email":    dt[4],
			"token":    dt[0],
		},
	}
	c.JSON(200, res)
	return
}

func CreateAdmin(c *gin.Context) {
	var a structs.CreateAdmin
	c.ShouldBind(&a)
	hashed_password, err := HashPassword(a.Password)
	if err != nil {
		c.JSON(200, gin.H{"status": "error", "detail": err})
		return
	}
	admin := database.Admin{Nama: a.Nama, Username: a.Username, Password: hashed_password, Email: a.Email, No_ponsel: a.No_ponsel, Alamat: a.Alamat}
	res := database.DB.Create(&admin)
	if res.Error != nil {
		c.JSON(200, gin.H{"status": "error", "detail": res.Error})
		return
	}
	c.JSON(200, gin.H{"status": "success", "detail": "Admin Created Successfully!"})
}

func GetAdminList(c *gin.Context) {
	admin := []structs.Admin{}
	admin_list := database.DB.Select([]string{"id", "nama", "username", "email", "no_ponsel", "alamat"}).Find(&admin)
	if admin_list.Error != nil {
		c.JSON(200, gin.H{"status": "error", "detail": admin_list.Error.Error()})
		return
	}
	c.JSON(200, gin.H{"status": "success", "data": &admin})
}

func GetAdmin(c *gin.Context) {
	var admin structs.Admin
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Fatal("error, ", err)
	}
	res := database.DB.Select("id", "nama", "username", "email", "no_ponsel", "alamat").Find(&admin, id)
	if res.Error != nil {
		c.String(200, "not found because: ", err)
	}
	c.JSON(200, gin.H{"status": "success", "data": &admin})
}

func DeleteAdmin(c *gin.Context) {
	var admin structs.Admin
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Fatal("error, ", err)
	}
	res := database.DB.Delete(&admin, id)
	if res.Error != nil {
		c.JSON(200, gin.H{"status": "error", "detail": res.Error.Error()})
	}
	c.JSON(200, gin.H{"status": "success", "data": "Admin deleted successfully"})
}
