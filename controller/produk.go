package controller

import (
	"brifice/database"
	"brifice/structs"
	"brifice/utils"
	"log"
	"math"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

type Produk struct{}
type ProdukCreate struct {
	NamaProdukInvestasi string  `json:"nama_produk"`
	DeskripsiProduk     string  `json:"deskripsi_produk"`
	Bunga               float64 `json:"bunga"`

	// Not Sure Up Border
	JenisProduk  []database.JenisProduk   `json:"jenis_produk"`
	ProfilResiko []database.Profil_Resiko `json:"profil_resiko"`
	// Not Sure Down Border
}
type ProdukResponse struct {
	ID                  uint      `json:"id_produk"`
	CreatedAt           time.Time `json:"registed"`
	NamaProdukInvestasi string    `json:"nama"`
	JenisProduk         string    `json:"jenis"`
	ProfilResiko        string    `json:"risiko"`
}

type CheckBox struct {
	ID   uint   `json:"id"`
	Nama string `json:"nama"`
}

func (p *Produk) Checkboxes(c *gin.Context) {
	var jp, pr []CheckBox
	q := database.DB.Model(&database.JenisProduk{}).Select("jenis_produk.id, jenis_produk.nama_jenis as nama").Find(&jp)
	if q.Error != nil {
		c.JSON(200, gin.H{"status": "error", "detail": "Jenis Produk Err: " + q.Error.Error()})
		return
	}
	q = database.DB.Model(&database.Profil_Resiko{}).Select("profil_resiko.id, profil_resiko.profil_resiko as nama").Find(&pr)
	if q.Error != nil {
		c.JSON(200, gin.H{"status": "error", "detail": "Profil Resiko Err: " + q.Error.Error()})
		return
	}
	SucResp(c, http.StatusOK, gin.H{"profil_resiko": &pr, "jenis_produk": &jp}, gin.H{"status": "success"})
	return
}

func (p *Produk) ListProduk(c *gin.Context) {
	var a []ProdukResponse
	pagination := utils.Paginate(c)
	totalData := database.DB.Find(&[]database.Produk_Investasi{}).RowsAffected
	queryBuilder := database.DB.Limit(pagination.Limit).Offset((pagination.Page - 1) * pagination.Limit).Order("created_at desc")

	//pg := utils.Paginate(c)
	q := queryBuilder.Debug().Model(&database.Produk_Investasi{}).Select("produk_investasi.id, produk_investasi.created_at, produk_investasi.nama_produk_investasi, string_agg(DISTINCT jp.nama_jenis, ', ') as jenis_produk, string_agg(DISTINCT r.profil_resiko, ', ') as profil_resiko").Group("produk_investasi.id, produk_investasi.nama_produk_investasi, produk_investasi.created_at").Joins("left join profile_resiko_produk pr on pr.produk_investasi_id = produk_investasi.id").Joins("left join profil_resiko r on r.id = pr.profil_resiko_id").Joins("left join jenis_produk_investasi j on j.produk_investasi_id = produk_investasi.id").Joins("left join jenis_produk jp on jp.id = j.jenis_produk_id").Scan(&a)
	if q.Error != nil {
		c.JSON(200, gin.H{"status": "error", "detail": q.Error})
		return
	}
	SucResp(c, 200, &a, gin.H{"page": pagination.Page, "total_data": totalData, "total_page": math.Ceil(float64(totalData) / float64(pagination.Limit))})
	return
}

func (p *Produk) CreateProduct(c *gin.Context) {
	var pc ProdukCreate
	c.ShouldBind(&pc)
	produk := database.Produk_Investasi{
		Nama_produk_investasi: pc.NamaProdukInvestasi,
		Deskripsi_produk:      pc.DeskripsiProduk,
		Bunga:                 pc.Bunga,
		JenisProduk:           pc.JenisProduk,
		Profil_resiko:         pc.ProfilResiko,
	}
	res := database.DB.Create(&produk)
	if res.Error != nil {
		c.JSON(500, gin.H{"status": "error", "detail": res.Error.Error()})
		return
	}
	c.JSON(200, gin.H{"status": "success", "detail": "product created successfully"})
	return
}

func CreateInvestmentProduk(c *gin.Context) {
	var p structs.CreateProduct
	c.ShouldBind(&p)
	product := database.Produk_Investasi{Nama_produk_investasi: p.Nama_produk, Jenis_produk: p.Jenis_produk, Simulasi_manfaat_iuran: p.Simulasi_manfaat_iuran, Deskripsi_produk: p.Deskripsi_produk}
	res := database.DB.Create(&product)
	if res.Error != nil {
		c.JSON(200, gin.H{"status": "error", "detail": res.Error.Error()})
		return
	}
	c.JSON(200, gin.H{"status": "success", "detail": "Produk " + p.Nama_produk + " berhasil dibuat!"})
}

func GetInvestmentProduk(c *gin.Context) {
	p_list := []structs.Produk_Investasi{}
	p := database.DB.Where("deleted_at IS NULL").Select([]string{"id", "nama_produk_investasi", "jenis_produk", "simulasi_manfaat_iuran", "deskripsi_produk"}).Find(&p_list)
	if p.Error != nil {
		c.JSON(200, gin.H{"status": "error", "detail": p.Error.Error()})
		return
	}
	c.JSON(200, gin.H{"status": "success", "data": &p_list})
}

func GetInvestmentProdukById(c *gin.Context) {
	var produk structs.Produk_Investasi
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Println("parsing id error")
	}
	res := database.DB.Select("id", "nama_produk_investasi", "jenis_produk", "simulasi_manfaat_iuran", "deskripsi_produk").Find(&produk, id)
	if res.Error != nil {
		c.JSON(200, gin.H{"status": "error", "detail": "not found: " + res.Error.Error()})
		return
	}
	c.JSON(200, gin.H{"status": "error", "data": &produk})
}

func UpdateInvestmentProduk(c *gin.Context) {
	//var produk database.Produk_Investasi
	var p structs.CreateProduct
	c.ShouldBind(&p)
	id, err := strconv.Atoi(c.Param("id"))
	//ad := c.Param("id")
	if err != nil {
		c.JSON(200, gin.H{"status": "error", "detail": "parsing id error" + err.Error()})
		return
	}
	upd := database.DB.Where("id = ?", id).Updates(structs.Produk_Investasi{Nama_produk_investasi: p.Nama_produk, Jenis_produk: p.Jenis_produk, Deskripsi_produk: p.Deskripsi_produk})
	if upd.Error != nil {
		c.JSON(200, gin.H{"status": "error", "detail": "update error: " + upd.Error.Error()})
		return
	}
	c.JSON(200, gin.H{"status": "success", "detail": "product update success"})
}

func DeleteInvestmentProduk(c *gin.Context) {
	var produk database.Produk_Investasi
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(200, gin.H{"status": "error", "detail": "parsing id error" + err.Error()})
		return
	}
	del := database.DB.Delete(&produk, id)
	if del.Error != nil {
		c.JSON(200, gin.H{"status": "error", "detail": "product delete failed: " + del.Error.Error()})
	}
	c.JSON(200, gin.H{"status": "success", "detail": "Product delete success"})
}
