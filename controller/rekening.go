package controller

import (
	"brifice/database"
	"brifice/structs"

	"github.com/gin-gonic/gin"
)

func CreateRekening(c *gin.Context) {
	var b structs.CreateRekening
	c.ShouldBind(&b)
	rekening := database.Bank_Profile{Id_nasabah: b.Id_nasabah, No_rekening: b.No_rekening, Nama_rekening: b.Nama_rekening, Cabang_bank: b.Cabang_bank, Saldo: b.Saldo}
	res := database.DB.Create(&rekening)
	if res.Error != nil {
		c.JSON(200, gin.H{"status": "gagal", "detail": res.Error.Error()})
		return
	}
	c.JSON(200, gin.H{"status": "success", "detail": "Bank Profile Created Successfully!"})
}
