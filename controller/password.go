package controller

import (
	"brifice/database"
	"brifice/structs"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"golang.org/x/crypto/bcrypt"
)

func LoginHandler(username, password, role string) structs.AuthResponse {
	var usrnm, mail, pw string
	var id uint
	var APPNAME = "BRIFiCe"
	var TOKEN_EXP = time.Duration(168) * time.Hour
	var SIGNMETH = jwt.SigningMethodHS256
	var s structs.AuthResponse
	if role == "user" {
		n := database.Nasabah{}
		err := database.DB.Where("username = ? or email = ?", username, username).Select("id, username, email, password").Find(&n)
		id, usrnm, mail, pw = n.Id, n.Username, n.Email, n.Password
		if err.RowsAffected < 1 {
			s.Status = "error"
			s.Data = "username/email incorrect"
			return s
		}
		if err.Error != nil {
			s.Status = "error"
			s.Data = "username/email incorrect" + err.Error.Error()
			return s
		}
	} else if role == "admin" {
		a := database.Admin{}
		err := database.DB.Where("username = ? or email = ?", username, username).Select("id, username, email, password").Find(&a)
		id, usrnm, mail, pw = a.Id, a.Username, a.Email, a.Password
		if err.RowsAffected < 1 {
			s.Status = "error"
			s.Data = "username/email incorrect"
			return s
		}
		if err.Error != nil {
			s.Status = "error"
			s.Data = "username/email incorrect: " + err.Error.Error()
			return s
		}
	} else {
		s.Status = "error"
		s.Data = "Not registered"
		return s
	}
	log.Println("TES: ", s.Data)
	match := CheckPasswordHash(password, pw)
	if !match {
		s.Status = "error"
		s.Data = "Password Incorrect!"
		return s
	}
	claims := structs.UserClaim{
		StandardClaims: jwt.StandardClaims{
			Issuer:    APPNAME,
			ExpiresAt: time.Now().Add(TOKEN_EXP).Unix(),
			IssuedAt:  int64(time.Now().Unix()),
		},
		Id:       id,
		Username: usrnm,
		Email:    mail,
		Role:     role,
	}
	sign := jwt.NewWithClaims(SIGNMETH, claims)
	token, err := sign.SignedString([]byte(loadkey()))
	if err != nil {
		s.Status = "error"
		s.Data = "signing failed" + err.Error()
		return s
	}
	a := fmt.Sprint(id)
	s.Data = token + "," + a + "," + usrnm + "," + role + "," + mail
	s.Status = "success"
	return s
}

func UserDetail(tokenString string) string {
	var detail string
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if jwt.GetSigningMethod("HS256") != token.Method {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(loadkey()), nil
	})
	if err != nil {
		return err.Error()
	}
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		detail = fmt.Sprintf("%v,%v,%v", claims["username"], claims["role"], claims["id"])
		return detail
	} else {
		return err.Error()
	}

}

func Auth(c *gin.Context) {
	tokenString := c.Request.Header.Get("Authorization")
	if len(tokenString) == 0 {
		c.String(200, "No Token")
		c.Abort()
		return
	}
	if tokenString == "" {
		c.String(200, "No Token")
		c.Abort()
		return
	}
	tokenString = strings.Split(tokenString, "Bearer ")[1]
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if jwt.GetSigningMethod("HS256") != token.Method {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(loadkey()), nil
	})

	if token != nil && err == nil {
		//fmt.Println("token verified")
	} else {
		result := gin.H{
			"message": "not authorized",
			"error":   err.Error(),
		}
		c.JSON(http.StatusUnauthorized, result)
		c.Abort()
	}
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func Loadenv() string {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Err on load env file: \n", err)
	}
	dburl := os.Getenv("PGX_DB_URL")
	return dburl
}

func loadkey() string {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Err on load keyenv: ", err)
	}
	return os.Getenv("JWT_SECRET")
}
