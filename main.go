package main

import (
	"brifice/controller"
	"brifice/database"
	"os"

	"github.com/gin-gonic/gin"
	cors "github.com/rs/cors/wrapper/gin"
)

func main() {
	database.ConnectDatabase()
	router := gin.Default()
	router.Use(cors.AllowAll())
	router.POST("/admin/auth", controller.AdminLogin)
	router.POST("/user/auth", controller.UserLogin)
	router.Use(controller.Auth)

	admin := router.Group("/admin")
	{
		admin.POST("/", controller.CreateAdmin)
		admin.GET("/", controller.GetAdminList)
		admin.GET("/:id", controller.GetAdmin)
		admin.DELETE("/:id", controller.DeleteAdmin)
		admin.GET("/dashboard")
	}

	web := router.Group("/web")
	{
		dashboard := new(controller.Dashboard)
		web.GET("/", dashboard.Dashboard)
		web.GET("/dashboard/produk", dashboard.JumlahPengajuanByProduk)
		web.GET("/dashboard/baru", dashboard.PengajuanBaru)
		web.GET("/dashboard/total", dashboard.TotalPengajuan)
		web.GET("/dashboard/nasabah", dashboard.TotalPengguna)

		pengajuan := new(controller.Pengajuan)
		web.GET("/pengajuan", pengajuan.Home)
		//web.GET("/")

		nasabah := new(controller.Nasabah)
		web.GET("/nasabah", nasabah.GetAll)
		web.GET("/nasabah/:id", nasabah.GetDetail)
		web.GET("/nasabah/:id/analyze", nasabah.GetAnalisis)
		web.GET("/nasabah/:id/portofolio", nasabah.GetPortofolio)

		produk := new(controller.Produk)
		web.GET("/checkboxes", produk.Checkboxes)
		web.GET("/produk", produk.ListProduk)
		web.POST("/produk", produk.CreateProduct)
	}

	user := router.Group("/user")
	{
		user.POST("/", controller.CreateUser)
		user.GET("/", controller.GetUserList)
		user.DELETE("/:id", controller.DeleteUser)
		user.GET("/:id", controller.GetUser)
		//user.GET("/dashboard", controller.Dashboard)
		user.POST("/rekening", controller.CreateRekening)
		user.POST("/riskprofile", controller.UserRiskProfileUpdate)
		user.GET("/profile", controller.UserProfile)
	}

	produk := router.Group("/produk")
	{

		produk.GET("/", controller.GetInvestmentProduk)
		produk.GET("/:id", controller.GetInvestmentProdukById)
		produk.POST("/", controller.CreateInvestmentProduk)
		produk.PUT("/:id", controller.UpdateInvestmentProduk)
		produk.DELETE("/:id", controller.DeleteInvestmentProduk)
	}

	invest := router.Group("/invest")
	{
		invest.POST("/direct", controller.DirectInvestOption)
		invest.POST("/direct/:id", controller.DirectInvestOptionDetail)
		invest.POST("/analyze", controller.InitialAnalyze)
		invest.GET("/analyze/pre", controller.PreAdjustment)
		invest.POST("/analyze/adjust/:transaksi", controller.Adjustment)
		invest.POST("/analyze/post", controller.InvestAnalyze)
		invest.POST("/analyze/continue", controller.ContinueInvestAnalyze)
		invest.POST("/analyze/:id", controller.AnalyzeInvestOptionDetail)
		invest.POST("/apply", controller.ApplyInvestment)
	}

	porte := os.Getenv("DEVELOPMENT_ADDR")
	router.Run(":" + porte)
}
