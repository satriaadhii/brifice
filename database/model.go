package database

import (
	"brifice/structs"
	"log"
	"os"
	"time"

	"github.com/joho/godotenv"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

type Admin struct {
	gorm.Model
	Id                uint `gorm:"primaryKey"`
	Nama              string
	Username          string `gorm:"unique;not null"`
	Password          string
	Email             string `gorm:"unique;not null"`
	No_ponsel         string `gorm:"unique;not null"`
	Alamat            string
	Created_Timestamp time.Time `gorm:"autoCreateTime"`
}

type Nasabah struct {
	gorm.Model
	Id               uint `gorm:"primaryKey"`
	Nama             string
	Username         string `gorm:"unique;not null"`
	Password         string
	Email            string `gorm:"unique;not null"`
	No_ponsel        string `gorm:"unique;not null"`
	Alamat           string
	Jenis_kelamin    string
	Tanggal_lahir    time.Time
	Tanggal_pensiun  time.Time
	Profil_Resiko_Id *uint
	IsActive         bool
	Bank_Profiles    []Bank_Profile     `gorm:"foreignKey:Id_nasabah;references:Id"`
	Daftar_pengajuan []Daftar_Pengajuan `gorm:"foreignKey:Id_nasabah;references:Id"`
	//Hasil_kuesioner  Hasil_Kuesioner    `gorm:"foreignKey:Id_nasabah;references:Id"`
	//Produk_investasi []Produk_Investasi `gorm:"many2many:investasi_nasabah;"`
	Analyze         Analyze         `gorm:"foreignKey:Id_nasabah;references:Id"`
	Analyze_expense Analyze_expense `gorm:"foreignKey:Id_nasabah;references:Id"`
	Analyze_Income  Analyze_income  `gorm:"foreignKey:Id_nasabah;references:Id"`
}

type Bank_Profile struct {
	gorm.Model
	Id               uint `gorm:"primaryKey"`
	Id_nasabah       uint
	No_rekening      string
	Nama_rekening    string
	Cabang_bank      string
	Saldo            float64
	Transaksi_masuk  []Transaksi_Masuk  `gorm:"foreignKey:Id_bank;references:Id"`
	Transaksi_keluar []Transaksi_Keluar `gorm:"foreignKey:Id_bank;references:Id"`
}

type Trx_Code struct {
	gorm.Model
	Id             uint `gorm:"primaryKey"`
	Trx_Code       string
	Nama_Trx_Code  string
	Jenis_Trx_Code string
	//Transaksi_masuk  []Transaksi_Masuk  `gorm:"foreignKey:Id_trx_code;references:Id"`
	//Transaksi_keluar []Transaksi_Keluar `gorm:"foreignKey:Id_trx_code;references:Id"`
}

type Transaksi_Masuk struct {
	gorm.Model
	Id                    uint `'gorm:"primaryKey"`
	Id_bank               uint
	Id_trx_code           uint
	Transaction_timestamp time.Time `gorm:"autoCreateTime"`
	Trx_Code              string
	Remarks               string
	Nominal_kredit        uint
	Status                bool
	//	Analyze_income        []Analyze_Income `gorm:"many2many:Analyze_Transaksi_Masuk;"`
}

type Transaksi_Keluar struct {
	gorm.Model
	Id                    uint `gorm:"primaryKey"`
	Id_bank               uint
	Trx_Code              string
	Id_trx_code           uint
	Transaction_timestamp time.Time `gorm:"autoCreateTime"`
	Remarks               string
	Nominal_debet         uint
	Status                bool
	//	Analyze_expense       []Analyze_Expense `gorm:"many2many:Analyze_Transaksi_keluar"`
}

type Analyze struct {
	gorm.Model
	Id                uint `gorm:"primaryKey"`
	Id_nasabah        uint
	Average_expense   float64
	Average_income    float64
	Idle_money        float64
	Created_timestamp time.Time `gorm:"autoCreateTime"`
	Updated_timestamp time.Time `gorm:"autoUpdateTime"`
}

type Analyze_expense struct {
	gorm.Model
	Id                uint `gorm:"primaryKey"`
	Id_nasabah        uint
	Remarks           string
	Nominal           float32
	Created_timestamp time.Time `gorm:"autoCreateTime"`
}

type Analyze_income struct {
	gorm.Model
	Id                uint `gorm:"primaryKey"`
	Id_nasabah        uint
	Remarks           string
	Nominal           float32
	Created_timestamp time.Time `gorm:"autoCreateTime"`
}

type Produk_Investasi struct {
	gorm.Model
	Id                     uint `gorm:"primaryKey"`
	Nama_produk_investasi  string
	Jenis_produk           string
	JenisProduk            []JenisProduk `gorm:"many2many:jenis_produk_investasi"`
	Simulasi_manfaat_iuran string
	Deskripsi_produk       string
	Profil_resiko_id       int
	Profil_resiko          []Profil_Resiko `gorm:"many2many:profile_resiko_produk;"`
	Bunga                  float64
}

type JenisProduk struct {
	gorm.Model
	ID              uint `gorm:"primaryKey"`
	NamaJenis       string
	ProdukInvestasi []Produk_Investasi `gorm:"many2many:jenis_produk_investasi"`
}

type Daftar_Pengajuan struct {
	gorm.Model
	Id                  uint `gorm:"primaryKey"`
	Id_nasabah          uint
	Id_produk_investasi uint
	Requested_Timestamp time.Time `gorm:"autoCreateTime"`
	Jumlah_pengajuan    float64
	Status              int
	Pengajuan_Status    int
	//	Notifikasi          Notifikasi `gorm:"foreignKey:Id_daftar_pengajuan;references:Id"`
}

type Notifikasi struct {
	gorm.Model
	Id                  uint `gorm:"primaryKey"`
	Id_daftar_pengajuan uint
	Judul_notifikasi    string
	Detail              string
	Created_Timestamp   time.Time `gorm:"autoCreateTime"`
	Status              bool
}

type Profil_Resiko struct {
	gorm.Model
	Id                      uint `gorm:"primaryKey" json:"ID"`
	Profil_resiko           string
	Deskripsi_profil_resiko string
	Nasabah                 Nasabah            `gorm:"foreignKey:Profil_Resiko_Id;references:Id"`
	Produk_investasi        []Produk_Investasi `gorm:"many2many:profile_resiko_produk;"`
	//Produk_investasi        Produk_Investasi `gorm:"foreignKey:Profil_resiko_id;references:Id"`
	//Portofolio_rekomendasi  string
	//Hasil_kuesioner Hasil_Kuesioner `gorm:"foreignKey:Id_profil_resiko;references:Id"`
}

type Investasi_Nasabah struct {
	Nasabah_id          uint `gorm:"primaryKey"`
	Produk_investasi_id uint `gorm:"primaryKey"`
	CreatedAt           time.Time
	Status              bool
	Jumlah_investasi    uint
	Manfaat             uint
}

type Status_Pengajuan struct {
	gorm.Model
	Nama_status      string
	Deskripsi_status string
}

func (a *Admin) AdminToGetAdmin() structs.Admin {
	return structs.Admin{
		Id:        a.Id,
		Nama:      a.Nama,
		Username:  a.Username,
		Email:     a.Email,
		No_ponsel: a.No_ponsel,
		Alamat:    a.Alamat,
	}
}

func (a *Nasabah) NasabahToGetNasabah() structs.Nasabah {
	return structs.Nasabah{
		Id:        a.Id,
		Nama:      a.Nama,
		Username:  a.Username,
		Email:     a.Email,
		No_ponsel: a.No_ponsel,
		Alamat:    a.Alamat,
		IsActive:  a.IsActive,
	}
}

func (a *Analyze) AnalyzetoGetAnalyze() structs.Analyze {
	return structs.Analyze{
		Id:              a.Id,
		Id_nasabah:      a.Id_nasabah,
		Average_expense: a.Average_expense,
		Average_income:  a.Average_income,
		Idle_money:      a.Idle_money,
	}
}

func (a *Analyze_expense) AEtoAE() structs.Analyze_expense {
	return structs.Analyze_expense{
		Remarks: a.Remarks,
		Nominal: a.Nominal,
	}
}
func (a *Analyze_income) AItoAI() structs.Analyze_income {
	return structs.Analyze_income{
		Remarks: a.Remarks,
		Nominal: a.Nominal,
	}
}

func (a *Produk_Investasi) ProduktoGetProduk() structs.Produk_Investasi {
	return structs.Produk_Investasi{
		Id:                    a.Id,
		Nama_produk_investasi: a.Nama_produk_investasi,
		Jenis_produk:          a.Jenis_produk,
		Deskripsi_produk:      a.Deskripsi_produk,
	}
}

var DB *gorm.DB

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func ConnectDatabase() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Println("error ", err)
	}
	dbstr := os.Getenv("DEVELOPMENT_BRI")
	db, err := gorm.Open(postgres.Open(dbstr), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
	})
	if err != nil {
		log.Println("error db connecting: ", err)
	}
	/* 	err = db.SetupJoinTable(&Nasabah{}, "Produk_investasi", &Investasi_Nasabah{})
	   	if err != nil {
	   		log.Fatal("custom join table error", err.Error())
	   	} */
	db.AutoMigrate(&Admin{}, &Status_Pengajuan{}, &Profil_Resiko{}, &Nasabah{}, &Bank_Profile{}, &Trx_Code{}, &Transaksi_Masuk{}, &Analyze{}, &Transaksi_Keluar{}, &Produk_Investasi{}, &Daftar_Pengajuan{}, &Notifikasi{}, &Analyze_expense{}, &Analyze_income{}, &JenisProduk{})
	hashed, _ := HashPassword("brificemaster")
	hashed1, _ := HashPassword("P@ssw0rd")
	hashed2, _ := HashPassword("b4TTL3")
	admin := Admin{Username: "masteradmin", Nama: "Brifice Master Admin", Password: hashed, Email: "brificemaster@bri.co.id", No_ponsel: "027", Alamat: "private"}
	tugimin := Nasabah{Username: "tugimin05", Nama: "Tugimin", Password: hashed1, Email: "tugimin@gmail.com", No_ponsel: "08457382134", Alamat: "Jl. Sudirman Jakarta Pusat"}
	syaroja := Nasabah{Username: "syrj666", Nama: "syaroja", Password: hashed2, Email: "syaroja@gmail.com", No_ponsel: "083468239921", Alamat: "Jl. Keramat Jakarta Selatan"}

	//Tambah Profil Resiko
	db.Where(Profil_Resiko{Id: 1}).FirstOrCreate(&Profil_Resiko{Id: 1, Profil_resiko: "Konservatif", Deskripsi_profil_resiko: "Tujuan Anda berinvestasi adalah untuk mendapatkan pertumbuhan nilai investasi dan dapat cukup menerima risiko yang sangat kecil. Anda bersedia untuk berinvestasi di produk yang memiliki risiko yang sangat rendah. Jangka waktu investasi 0-2 tahun."})
	db.Where(Profil_Resiko{Id: 2}).FirstOrCreate(&Profil_Resiko{Id: 2, Profil_resiko: "Moderat", Deskripsi_profil_resiko: "Tujuan Anda berinvestasi adalah untuk mendapatkan pertumbuhan sedang dan dapat menerima risiko sedang. Anda bersedia untuk berinvestasi di produk yang memiliki risiko yang sedang. Jangka waktu investasi 2-3 tahun."})
	//db.Where(Profil_Resiko{Id: 3}).FirstOrCreate(&Profil_Resiko{Id: 3, Profil_resiko: "Moderat - Agresif", Deskripsi_profil_resiko: "Tujuan Anda berinvestasi adalah untuk mendapatkan pertumbuhan yang tinggi dan dapat toleransi terhadap risiko yang tinggi. Anda bersedia untuk berinvestasi di produk yang memiliki risiko yang sedang-tinggi. Jangka waktu investasi 3-5 tahun."})
	db.Where(Profil_Resiko{Id: 4}).FirstOrCreate(&Profil_Resiko{Id: 4, Profil_resiko: "Agresif", Deskripsi_profil_resiko: "Tujuan Anda berinvestasi adalah untuk mendapatkan pertumbuhan yang pesat dan dapat toleransi terhadap risiko yang tinggi. Anda bersedia untuk berinvestasi di produk yang memiliki risiko yang tinggi. Jangka waktu investasi > 5 tahun."})

	//Tambah Admin
	db.Where(Admin{Username: "masteradmin"}).FirstOrCreate(&admin)

	db.Where("id = ?", 1).FirstOrCreate(&JenisProduk{ID: 1, NamaJenis: "Reksadana Pasar Uang"})
	db.Where("id = ?", 2).FirstOrCreate(&JenisProduk{ID: 2, NamaJenis: "SBN"})
	db.Where("id = ?", 3).FirstOrCreate(&JenisProduk{ID: 3, NamaJenis: "Reksadana Pendapatan Tetap"})
	db.Where("id = ?", 4).FirstOrCreate(&JenisProduk{ID: 4, NamaJenis: "Reksadana Campuran"})
	db.Where("id = ?", 5).FirstOrCreate(&JenisProduk{ID: 5, NamaJenis: "Reksadana Saham"})

	//Tambah status pengajuan
	db.Where(Admin{Id: 1}).FirstOrCreate(&Status_Pengajuan{Nama_status: "submission", Deskripsi_status: "Pengajuan investasi menunggu konfirmasi dari Admin"})
	db.Where(Admin{Id: 2}).FirstOrCreate(&Status_Pengajuan{Nama_status: "forwarded", Deskripsi_status: "Pengajuan investasi diteruskan oleh Admin ke pihak Bank"})
	db.Where(Admin{Id: 3}).FirstOrCreate(&Status_Pengajuan{Nama_status: "approve", Deskripsi_status: "Pengajuan investasi telah disetujui oleh Admin dan Pihak Bank"})
	db.Where(Admin{Id: 4}).FirstOrCreate(&Status_Pengajuan{Nama_status: "rejected", Deskripsi_status: "Pengajuan investasi ditolak oleh admin dan pihak Bank"})

	//Tambah Dummy User
	db.Where(Nasabah{Username: "tugimin05"}).FirstOrCreate(&tugimin)
	db.Where(Nasabah{Username: "syrj666"}).FirstOrCreate(&syaroja)

	//Tambah Produk Investasi
	db.Where(Produk_Investasi{Nama_produk_investasi: "Britama Rencana"}).FirstOrCreate(&Produk_Investasi{Nama_produk_investasi: "Britama Rencana", Jenis_produk: "Reksadana Pendapatan Tetap", Deskripsi_produk: "Bahana Makara Prima adalah investasi berjenis Reksadana Pendapatan Tetap dengan minimal investasi senilai Rp. 1.000.000 rupiah"})
	db.Where(Produk_Investasi{Nama_produk_investasi: "Deposito"}).FirstOrCreate(&Produk_Investasi{Nama_produk_investasi: "Deposito", Jenis_produk: "Reksadana Pasar Uang", Deskripsi_produk: "Schroder Dana Likuid adalah investasi berjenis Reksadana Pasar Uang dengan minimal investasi senilai Rp. 10.000 rupiah"})
	db.Where(Produk_Investasi{Nama_produk_investasi: "DPLK"}).FirstOrCreate(&Produk_Investasi{Nama_produk_investasi: "DPLK", Jenis_produk: "SBN", Deskripsi_produk: "Sukuk Tabungan adalah investasi berjenis SBN dengan minimal investasi senilai Rp. 100.000 rupiah"})
	db.Where(Produk_Investasi{Nama_produk_investasi: "Reksadana"}).FirstOrCreate(&Produk_Investasi{Nama_produk_investasi: "Reksadana", Jenis_produk: "SBN", Deskripsi_produk: "Sukuk Tabungan adalah investasi berjenis SBN dengan minimal investasi senilai Rp. 100.000 rupiah"})

	// Uncomment for the 1st time run
	//Tambah Profile Resiko to Produk
	/* 	var britama, deposito, dplk, Reksadana Produk_Investasi
	   	var r1, r2, r3 Profil_Resiko
	   	var j1, j2, j3, j4, j5 JenisProduk
	   	db.Find(&britama, 1)
	   	db.Find(&deposito, 2)
	   	db.Find(&dplk, 3)
	   	db.Find(&Reksadana, 5)
	*/ // append resiko relation
	/* db.Find(&r1, 1)
	db.Find(&r2, 2)
	db.Find(&r3, 4)
	db.Model(&britama).Association("Profil_resiko").Append([]Profil_Resiko{r1, r2, r3})
	db.Model(&deposito).Association("Profil_resiko").Append([]Profil_Resiko{r1, r2, r3})
	db.Model(&dplk).Association("Profil_resiko").Append([]Profil_Resiko{r2, r3})
	db.Model(&Reksadana).Association("Profil_resiko").Append([]Profil_Resiko{r3})
	*/
	// append jenis produk relation
	/* 	db.Find(&j1, 1)
	   	db.Find(&j2, 2)
	   	db.Find(&j3, 3)
	   	db.Find(&j4, 4)
	   	db.Find(&j5, 5)
	   	db.Model(&britama).Association("JenisProduk").Append([]JenisProduk{j1, j3, j5})
	   	db.Model(&deposito).Association("JenisProduk").Append([]JenisProduk{j1, j3, j5})
	   	db.Model(&dplk).Association("JenisProduk").Append([]JenisProduk{j2})
	   	db.Model(&Reksadana).Association("JenisProduk").Append([]JenisProduk{j3})
	*/DB = db
}
