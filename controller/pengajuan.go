package controller

import (
	"brifice/database"
	"brifice/utils"
	"math"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type Pengajuan struct{}
type PengajuanResponse struct {
	RequestedTimestamp  time.Time `json:"timestamp"`
	Nama                string    `json:"nama"`
	NamaProdukInvestasi string    `json:"produk_investasi"`
	JumlahPengajuan     float64   `json:"jumlah_pengajuan"`
	NamaStatus          string    `json:"status"`
}

func (p *Pengajuan) Home(c *gin.Context) {
	var a []PengajuanResponse
	var err *gorm.DB
	search := c.Query("search")
	pagination := utils.Paginate(c)
	totalData := database.DB.Find(&[]database.Daftar_Pengajuan{}).RowsAffected
	queryBuilder := database.DB.Limit(pagination.Limit).Offset((pagination.Page - 1) * pagination.Limit).Order("requested_timestamp desc")
	if search == "" {
		err = queryBuilder.Debug().Model(&database.Daftar_Pengajuan{}).Select("daftar_pengajuan.requested_timestamp, nasabah.nama, produk_investasi.nama_produk_investasi, daftar_pengajuan.jumlah_pengajuan, status_pengajuan.nama_status").Joins("left join nasabah on nasabah.id = daftar_pengajuan.id_nasabah").Joins("left join produk_investasi on produk_investasi.id = daftar_pengajuan.id_produk_investasi").Joins("left join status_pengajuan on status_pengajuan.id = daftar_pengajuan.pengajuan_status").Scan(&a)
	} else {
		err = queryBuilder.Debug().Model(&database.Daftar_Pengajuan{}).Where("nasabah.nama ILIKE ? OR produk_investasi.nama_produk_investasi ILIKE ? OR status_pengajuan.nama_status ILIKE ?", "%"+search+"%", "%"+search+"%", "%"+search+"%").Select("daftar_pengajuan.requested_timestamp, nasabah.nama, produk_investasi.nama_produk_investasi, daftar_pengajuan.jumlah_pengajuan, status_pengajuan.nama_status").Joins("left join nasabah on nasabah.id = daftar_pengajuan.id_nasabah").Joins("left join produk_investasi on produk_investasi.id = daftar_pengajuan.id_produk_investasi").Joins("left join status_pengajuan on status_pengajuan.id = daftar_pengajuan.pengajuan_status").Scan(&a)
	}
	if err.Error != nil {
		c.JSON(200, gin.H{"status": "gagal", "detail": err.Error.Error()})
		return
	}
	res := gin.H{"pengajuan": &a}
	SucResp(c, 200, &res, gin.H{"page": pagination.Page, "total_data": totalData, "total_page": math.Ceil(float64(totalData) / float64(pagination.Limit))})
	return
}
