package controller

import (
	"brifice/database"
	"brifice/structs"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

func UserLogin(c *gin.Context) {
	var n structs.Auth
	c.ShouldBind(&n)
	t := LoginHandler(n.Username, n.Password, "user")
	if t.Status == "error" {
		res := gin.H{"status": t.Status, "detail": t.Data}
		c.JSON(200, res)
		return
	}
	dt := strings.Split(t.Data, ",")
	id, err := strconv.Atoi(dt[1])
	if err != nil {
		log.Println("parsing gagal, kemungkinan user tidak ada")
	}
	var res = gin.H{
		"status": "success",
		"data": gin.H{
			"id":       id,
			"username": dt[2],
			"role":     dt[3],
			"email":    dt[4],
			"token":    dt[0],
		},
	}
	c.JSON(200, res)
}

func CreateUser(c *gin.Context) {
	var a structs.CreateNasabah
	c.ShouldBind(&a)
	hashed_password, err := HashPassword(a.Password)
	if err != nil {
		c.JSON(200, gin.H{"status": "error", "detail": err})
		return
	}
	nasabah := database.Nasabah{Nama: a.Nama, Username: a.Username, Password: hashed_password, Email: a.Email, No_ponsel: a.No_ponsel, Alamat: a.Alamat, IsActive: true}
	res := database.DB.Create(&nasabah)
	if res.Error != nil {
		c.JSON(200, gin.H{"status": "error", "detail": res.Error.Error()})
		return
	}
	c.JSON(200, gin.H{"status": "success", "detail": "User Created Successfully!"})
}

func GetUserList(c *gin.Context) {
	nasabah := []structs.Nasabah{}
	nasabah_list := database.DB.Where("is_active = ?", true).Select([]string{"id", "nama", "username", "email", "no_ponsel", "alamat", "is_active"}).Find(&nasabah)
	if nasabah_list.Error != nil {
		c.JSON(200, gin.H{"status": "error", "detail": nasabah_list.Error.Error()})
		return
	}
	c.JSON(200, gin.H{"status": "success", "data": &nasabah})
}

func GetUser(c *gin.Context) {
	var nasabah structs.Nasabah
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(200, gin.H{"status": "error", "detail": err})
		return
	}
	res := database.DB.Select("id", "nama", "username", "email", "no_ponsel", "alamat", "is_active").Find(&nasabah, id)
	if res.Error != nil {
		c.JSON(200, gin.H{"status": "error", "detail": res.Error.Error()})
	}
	c.JSON(200, gin.H{"status": "success", "data": &nasabah})
}

func DeleteUser(c *gin.Context) {
	var nasabah structs.Nasabah
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(200, gin.H{"status": "error", "detail": err.Error()})
	}
	res := database.DB.First(&nasabah, id).Update("is_active", false)
	if res.Error != nil {
		c.JSON(200, gin.H{"status": "error", "detail": res.Error.Error()})
	}
	c.JSON(200, gin.H{"status": "success", "data": "Nasabah deleted successfully"})
}

func Analyze(c *gin.Context) {
	tokenString := c.Request.Header.Get("Authorization")
	tokenString = strings.Split(tokenString, "Bearer ")[1]
	detail := strings.Split(UserDetail(tokenString), ",")
	id, err := strconv.ParseUint(detail[2], 10, 32)
	if err != nil {
		log.Println("Err : ", err.Error())
	}
	tk, tm := Mutasi(uint(id))
	r := PreAnalyze(tk, tm, id)
	c.JSON(200, &r)
	return
}

func PreAnalyzeAdjustment(c *gin.Context) {
	transaksi := c.Query("transaksi")
	three_month := time.Now().AddDate(0, -3, 0)
	tokenString := c.Request.Header.Get("Authorization")
	tokenString = strings.Split(tokenString, "Bearer ")[1]
	detail := strings.Split(UserDetail(tokenString), ",")
	id, _ := strconv.ParseUint(detail[2], 10, 32)
	n := []database.Nasabah{}
	pa := []structs.PreAdjust{}
	if transaksi == "masuk" {
		database.DB.Model(&n).Where("transaksi_masuk.transaction_timestamp > ? AND nasabah.id = ?", three_month, id).Select("transaksi_masuk.id, transaksi_masuk.remarks, transaksi_masuk.status").Joins("JOIN bank_profile on bank_profile.id_nasabah = nasabah.id").Joins("JOIN transaksi_masuk on transaksi_masuk.id_bank = bank_profile.id").Find(&pa)
		c.JSON(200, &pa)
		return
	} else if transaksi == "keluar" {
		database.DB.Model(&n).Where("transaksi_keluar.transaction_timestamp > ? AND nasabah.id = ?", three_month, id).Select("transaksi_keluar.id, transaksi_keluar.remarks, transaksi_keluar.status").Joins("JOIN bank_profile on bank_profile.id_nasabah = nasabah.id").Joins("JOIN transaksi_keluar on transaksi_keluar.id_bank = bank_profile.id").Find(&pa)
		c.JSON(200, &pa)
		return
	}
}

/* func AnalyzeAdjustment(c *gin.Context) {
	var a []structs.AnalyzeItem
	c.ShouldBind(&a)
	transaksi := c.Param("transaksi")

	if transaksi == "masuk" {
		log.Println("masuk")
		tm := database.Transaksi_Masuk{}
		for _, ai := range a {
			err := database.DB.Model(&tm).Where("id = ?", ai.Id).Update("status", ai.Status)
			if err.Error != nil {
				c.JSON(200, gin.H{"status": "error", "detail": err.Error.Error()})
				return
			}
		}
		c.JSON(200, gin.H{"status": "success", "detail": "penyesuaian berhasil!"})

	} else if transaksi == "keluar" {
		log.Println("keluar")
		tk := database.Transaksi_Keluar{}
		for _, ai := range a {
			err := database.DB.Model(&tk).Where("id = ?", ai.Id).Update("status", ai.Status)
			if err.Error != nil {
				c.JSON(200, gin.H{"status": "error", "detail": err.Error.Error()})
				return
			}
		}
		c.JSON(200, gin.H{"status": "success", "detail": "penyesuaian berhasil!"})
	}
} */
func getId(c *gin.Context) (uint64, error) {
	tokenString := c.Request.Header.Get("Authorization")
	tokenString = strings.Split(tokenString, "Bearer ")[1]
	detail := strings.Split(UserDetail(tokenString), ",")
	id, err := strconv.ParseUint(detail[2], 10, 32)
	if err != nil {
		log.Println("Err : ", err.Error())
	}
	return id, err
}

func UserRiskProfileUpdate(c *gin.Context) {
	tokenString := c.Request.Header.Get("Authorization")
	tokenString = strings.Split(tokenString, "Bearer ")[1]
	detail := strings.Split(UserDetail(tokenString), ",")
	id, err := strconv.ParseUint(detail[2], 10, 32)
	if err != nil {
		log.Println("Id parsing error: ", err.Error())
	}
	var p structs.NilaiKuesioner
	var n database.Nasabah
	c.ShouldBind(&p)
	var profil_resiko int
	switch {
	case p.Nilai < 12:
		profil_resiko = 1
	case p.Nilai < 23:
		profil_resiko = 2
	case p.Nilai >= 23:
		profil_resiko = 4
	default:
		{
			log.Println("gaada coy")
		}
	}

	res := database.DB.Model(&n).Where("id = ?", id).Update("profil_resiko_id", profil_resiko)
	if res.Error != nil {
		c.JSON(200, gin.H{"status": "error", "detail": "yntkts" + res.Error.Error()})
		return
	}

	var pr database.Profil_Resiko
	res = database.DB.Find(&pr, profil_resiko)

	c.JSON(200, gin.H{"status": "success", "detail": gin.H{"profil_resiko": pr.Profil_resiko, "deskripsi": pr.Deskripsi_profil_resiko}})

}

func UserProfile(c *gin.Context) {
	id, err := getId(c)
	if err != nil {
		c.JSON(200, err)
		return
	}
	n, p, b, r := database.Nasabah{}, structs.UserProfile{}, []database.Bank_Profile{}, database.Profil_Resiko{}
	//db := database.DB.Model(&n).Select("nama, profil_resiko").Joins("JOIN profil_resiko on profil_resiko.id = nasabah.profil_resiko_id").Find(&p, id)

	db := database.DB.Select("nama, profil_resiko_id").Find(&n, id)

	if db.Error != nil {
		c.JSON(200, ErrorResp(db.Error.Error()))
		return
	}
	if db.RowsAffected == 0 {
		c.JSON(200, ErrorResp("User Not Found!"))
		return
	}

	db = database.DB.Where("id = ?", n.Profil_Resiko_Id).Select("profil_resiko").Find(&r)

	db = database.DB.Where("id_nasabah = ?", id).Select("no_rekening").Find(&b)
	if db.Error != nil {
		c.JSON(200, ErrorResp(db.Error.Error()))
		return
	}
	if db.RowsAffected == 0 {
		p.Nama = n.Nama
		p.Profil_Resiko = r.Profil_resiko
		c.JSON(200, gin.H{"status": "success", "detail": &p})
		//c.JSON(200, ErrorResp("Rekening Not Found!"))
		return
	}

	if len(b) > 1 {
		for _, b2 := range b {
			p.No_rekening = append(p.No_rekening, b2.No_rekening)
		}
		p.Nama = n.Nama
		p.Profil_Resiko = r.Profil_resiko
		c.JSON(200, gin.H{"detail": &p})
		return
	} else {
		usp := structs.USP{}
		usp.Nama = n.Nama
		usp.Profil_Resiko = r.Profil_resiko
		for _, b1 := range b {
			usp.No_rekening = b1.No_rekening
		}
		c.JSON(200, gin.H{"status": "success", "detail": &usp})
		return
	}
}

/* func Dashboard(c *gin.Context) {
	tokenString := c.Request.Header.Get("Authorization")
	tokenString = strings.Split(tokenString, "Bearer ")[1]
	detail := strings.Split(UserDetail(tokenString), ",")
	username, _ := detail[0], detail[1]
	var nasabah structs.Nasabah
	res := database.DB.Where("username = ?", username).Select("nama").Find(&nasabah)
	if res.Error != nil {
		c.JSON(200, gin.H{"status": "error", "detail": res.Error.Error()})
		return
	}
	c.JSON(200, gin.H{"status": "success", "data": &nasabah.Nama})
}
*/
