package structs

import (
	"github.com/dgrijalva/jwt-go"
)

type Transaksi struct {
	Remarks string  `json:"remarks"`
	Nominal float32 `json:"nominal"`
	Total   int     `json:"total"`
}
type Norek struct {
	NomorRekening string
}

type Analyze_create struct {
	Remarks    string
	Nominal    float32
	Id_nasabah uint64
}

type Pengajuan_nasabah struct {
	Jumlah_investasi float64 `json:"jumlah_investasi"`
	Id_produk        uint    `json:"id_produk"`
}

type Admin struct {
	Id        uint   `json:"id_admin"`
	Nama      string `json:"nama"`
	Username  string `json:"username"`
	Email     string `json:"email"`
	No_ponsel string `json:"no_ponsel"`
	Alamat    string `json:"alamat"`
}

type CreateAdmin struct {
	Nama      string `json:"nama" form:"nama"`
	Username  string `json:"username" form:"username"`
	Password  string `json:"password" form:"password"`
	Email     string `json:"email" form:"email"`
	No_ponsel string `json:"no_ponsel" form:"no_ponsel"`
	Alamat    string `json:"alamat" form:"alamat"`
}

type Result struct {
	Id       string  `json:"id"`
	Remarks  string  `json:"remarks"`
	Trx_code string  `json:"trx_code"`
	Nominal  float32 `json:"nominal"`
}

type FinalResult struct {
	Id       string  `json:"id"`
	Remarks  string  `json:"remarks"`
	Percent  float32 `json:"persen"`
	Trx_code string  `json:"trx_code"`
	//Nominal  float32 `json:"nominal"`
}

type FinRes struct {
	Remarks string  `json:"remarks"`
	Percent float32 `json:"persen"`
	Nominal float32 `json:"nominal"`
}

type AnalyzeInvOpt struct {
	Id                    uint   `json:"id"`
	Nama_produk_investasi string `json:"nama_produk"`
	Jenis_produk          string `json:"jenis_produk"`
	Deskripsi_produk      string `json:"deskripsi_produk"`
	//Profil_resiko         uint    `json:"profil_resiko"`
	Idle_money uint `json:"idle_money"`
	Total_1    uint `json:"total_1"`
	Total_2    uint `json:"total_2"`
	Total_3    uint `json:"total_3"`
	//Total_30   float64 `json:"total_30"`
}

type UserProfile struct {
	Nama          string   `json:"nama"`
	No_rekening   []string `json:"nomor_rekening"`
	Profil_Resiko string   `json:"profil_resiko"`
}

type USP struct {
	Nama          string `json:"nama"`
	No_rekening   string `json:"nomor_rekening"`
	Profil_Resiko string `json:"profil_resiko"`
}

type PreAdjust struct {
	Id      uint   `json:"id"`
	Remarks string `json:"remarks"`
	Status  bool   `json:"status"`
}

type AnalyzeItem struct {
	Id     uint `json:"id"`
	Status bool `json:"status"`
}

type CreateAnalyze struct {
	Average_expense float64 `json:"average_expense"`
	Average_income  float64 `json:"average_income"`
	Idle_money      float64 `json:"idle_money"`
}

type CreateRekening struct {
	Id_nasabah    uint    `json:"id_nasabah" form:"id_nasabah"`
	No_rekening   string  `json:"no_rekening" form:"no_rekening"`
	Nama_rekening string  `json:"nama_rekening" form:"nama_rekening"`
	Cabang_bank   string  `json:"cabang_bank" form:"cabang_bank"`
	Saldo         float64 `json:"saldo" form:"saldo"`
}

type Auth struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type UserClaim struct {
	jwt.StandardClaims
	Id       uint   `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
	Email    string `json:"email"`
	Role     string `json:"role"`
}

type AuthResponse struct {
	Status string `json:"status"`
	Data   string `json:"data"`
}

type AuthData struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
	Role     string `json:"role"`
	Email    string `json:"email"`
	Token    string `json:"token"`
}

type AuthRes struct {
	Status string   `json:"status"`
	Data   AuthData `json:"data"`
}

type Analyze struct {
	Id              uint    `json:"id"`
	Id_nasabah      uint    `json:"id_nasabah"`
	Average_expense float64 `json:"average_expense"`
	Average_income  float64 `json:"average_income"`
	Idle_money      float64 `json:"idle_money"`
}

type Nasabah struct {
	Id        uint   `json:"id"`
	Nama      string `json:"nama"`
	Username  string `json:"username"`
	Email     string `json:"email"`
	No_ponsel string `json:"no_ponsel"`
	Alamat    string `json:"alamat"`
	IsActive  bool   `json:"aktif"`
}

type Analyze_income struct {
	Remarks string  `json:"remarks"`
	Nominal float32 `json:"nominal"`
}

type Analyze_expense struct {
	Remarks string  `json:"remarks"`
	Nominal float32 `json:"nominal"`
}

type Produk_Investasi struct {
	Id                    uint   `json:"id"`
	Nama_produk_investasi string `json:"nama_produk"`
	Jenis_produk          string `json:"jenis_produk"`
	//Simulasi_manfaat_produk string `json:"simulasi_manfaat"`
	Deskripsi_produk string `json:"deskripsi_produk"`
	//Profil_Resiko_Id        []uint `json:"profil_resiko_id"`
}

type ErrResponse struct {
	Status string `json:"status"`
	Detail string `json:"detail"`
}

type CreateNasabah struct {
	Nama      string `json:"nama" form:"nama"`
	Username  string `json:"username" form:"username"`
	Password  string `json:"password" form:"password"`
	Email     string `json:"email" form:"email"`
	No_ponsel string `json:"no_ponsel" form:"no_ponsel"`
	Alamat    string `json:"alamat" form:"alamat"`
}

type Nasabah_Profile struct {
	Nama          string
	Profil_resiko uint
}

type CreateProduct struct {
	Nama_produk            string `json:"nama_produk" form:"nama_produk"`
	Jenis_produk           string `json:"jenis_produk" form:"jenis_produk"`
	Simulasi_manfaat_iuran string `json:"simulasi_manfaat" form:"simulasi_manfaat"`
	Deskripsi_produk       string `json:"deskripsi_produk" form:"deskripsi_produk"`
}

type NilaiKuesioner struct {
	Nilai int `json:"nilai"`
}

type InvestDetail struct {
	Jumlah_investasi float64 `json:"jumlah_investasi"`
}
