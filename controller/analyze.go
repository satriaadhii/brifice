package controller

import (
	"brifice/database"
	"brifice/structs"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

func Mutasi2(id_nasabah uint) ([]structs.Result, []structs.Result) {
	tm, tk := []structs.Result{}, []structs.Result{}
	n := []database.Nasabah{}
	database.DB.Model(&n).Where("nasabah.id = ?", id_nasabah).Select("transaksi_keluar.id, transaksi_keluar.remarks, transaksi_keluar.trx_code, transaksi_keluar.nominal_debet as Nominal").Joins("JOIN bank_profile on bank_profile.id_nasabah = nasabah.id").Joins("JOIN transaksi_keluar on transaksi_keluar.id_bank = bank_profile.id").Find(&tk)
	database.DB.Model(&n).Where("nasabah.id = ?", id_nasabah).Select("transaksi_masuk.id, transaksi_masuk.remarks, transaksi_masuk.trx_code, transaksi_masuk.nominal_kredit as Nominal").Joins("JOIN bank_profile on bank_profile.id_nasabah = nasabah.id").Joins("JOIN transaksi_masuk on transaksi_masuk.id_bank = bank_profile.id").Find(&tm)
	return tk, tm
}

func InvestAnalyze(c *gin.Context) {
	id, err := getId(c)
	if err != nil {
		log.Println("ERR: ", err.Error())
	}
	var an database.Analyze
	var a structs.Analyze
	var ca structs.CreateAnalyze
	c.ShouldBind(&ca)
	//log.Println("tr: ", ca.Average_expense, "tm: ", ca.Average_income, "im: ", ca.Idle_money)
	result := database.DB.Where("id_nasabah = ?", id).First(&an)
	if result.RowsAffected < 1 {
		//log.Println("Belum ada, coba buat baru, ROW: ", result.RowsAffected)
		an = database.Analyze{Id_nasabah: uint(id), Average_expense: ca.Average_expense, Average_income: ca.Average_income, Idle_money: ca.Idle_money}
		database.DB.Create(&an)
	} else {
		//log.Println("Sudah ada, edit")
		database.DB.Where("id_nasabah = ?", id).Updates(database.Analyze{Average_expense: ca.Average_expense, Average_income: ca.Average_income, Idle_money: ca.Idle_money})
	}

	database.DB.Where("id_nasabah = ?", id).First(&a)
	res := InvOpt(id, a.Idle_money)

	c.JSON(200, res)
}

func ContinueInvestAnalyze(c *gin.Context) {
	var a structs.Analyze
	id, err := getId(c)
	if err != nil {
		log.Println("ERR: ", err.Error())
	}
	database.DB.Where("id_nasabah", id).First(&a)
	res := InvOpt(id, a.Idle_money)
	c.JSONP(200, &res)
}

func AnalyzeInvestOptionDetail(c *gin.Context) {
	var n structs.InvestDetail
	id, err := getId(c)
	if err != nil {
		log.Println("ERR: ", err.Error())
	}
	id_produk, err := strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil {
		c.JSON(200, gin.H{"status": "error", "detail": "conrter error"})
		return
	}
	c.ShouldBind(&n)
	p := InvOptDetail(id, id_produk, n.Jumlah_investasi)
	c.JSON(200, gin.H{"status": "success", "data": p["produk"]})
	return
}

func GetDuplicateRemarks(tr []structs.Result) map[string]structs.Transaksi {
	var tra = map[string]structs.Transaksi{}
	//var value, isExist = tra["adad"]
	transaksi := structs.Transaksi{}
	for _, item := range tr {
		if len(tra) == 0 {
			transaksi.Nominal = item.Nominal
			transaksi.Remarks = item.Remarks
			transaksi.Total = 1
			tra[item.Remarks] = transaksi
		} else {
			value, isExist := tra[item.Remarks]
			if isExist {
				value.Nominal = value.Nominal + item.Nominal
				value.Total = value.Total + 1
				tra[item.Remarks] = value
			} else {
				transaksi.Nominal = item.Nominal
				transaksi.Remarks = item.Remarks
				transaksi.Total = 1
				tra[item.Remarks] = transaksi
			}
		}
	}
	return tra
}

func DeleteBelow3(a map[string]structs.Transaksi) map[string]structs.Transaksi {
	for _, t := range a {
		if t.Total < 3 {
			delete(a, t.Remarks)
		}
	}
	return a
}

func preWrite(a map[string]structs.Transaksi, idnas uint64, jenis string) {
	if jenis == "keluar" {
		var ca []database.Analyze_expense
		var c database.Analyze_expense
		for _, t := range a {
			c.Id_nasabah = uint(idnas)
			c.Nominal = t.Nominal
			c.Remarks = t.Remarks
			ca = append(ca, c)
		}
		//log.Println(ca)
		database.DB.CreateInBatches(&ca, 10)
	}
	if jenis == "masuk" {
		var ca []database.Analyze_income
		var c database.Analyze_income
		for _, t := range a {
			c.Id_nasabah = uint(idnas)
			c.Nominal = t.Nominal
			c.Remarks = t.Remarks
			ca = append(ca, c)
		}
		//log.Println(ca)
		database.DB.CreateInBatches(&ca, 10)
	}
}

func PreAdjustment(c *gin.Context) {
	transaksi := c.Query("transaksi")
	//three_month := time.Now().AddDate(0, -3, 0)
	tokenString := c.Request.Header.Get("Authorization")
	tokenString = strings.Split(tokenString, "Bearer ")[1]
	detail := strings.Split(UserDetail(tokenString), ",")
	id, _ := strconv.ParseUint(detail[2], 10, 32)
	n := []database.Nasabah{}
	pa := []structs.Result{}
	if transaksi == "masuk" {
		database.DB.Model(&n).Where( /* "transaksi_masuk.transaction_timestamp > ? AND */ "nasabah.id = ?" /* , three_month */, id).Select("transaksi_masuk.id, transaksi_masuk.remarks, transaksi_masuk.trx_code, transaksi_masuk.nominal_kredit as Nominal").Joins("JOIN bank_profile on bank_profile.id_nasabah = nasabah.id").Joins("JOIN transaksi_masuk on transaksi_masuk.id_bank = bank_profile.id").Find(&pa)
		mp := GetDuplicateRemarks(pa)
		res := MapToSlice(mp)
		c.JSON(200, &res)
		return
	} else if transaksi == "keluar" {
		database.DB.Model(&n).Where( /* "transaksi_keluar.transaction_timestamp > ? AND */ "nasabah.id = ?" /* , three_month */, id).Select("transaksi_keluar.id, transaksi_keluar.remarks, transaksi_keluar.trx_code, transaksi_keluar.nominal_debet as Nominal").Joins("JOIN bank_profile on bank_profile.id_nasabah = nasabah.id").Joins("JOIN transaksi_keluar on transaksi_keluar.id_bank = bank_profile.id").Find(&pa)
		mp := GetDuplicateRemarks(pa)
		res := MapToSlice(mp)
		c.JSON(200, &res)
		return
	}
}

func Adjustment(c *gin.Context) {
	tokenString := c.Request.Header.Get("Authorization")
	tokenString = strings.Split(tokenString, "Bearer ")[1]
	detail := strings.Split(UserDetail(tokenString), ",")
	id, err := strconv.ParseUint(detail[2], 10, 32)

	if err != nil {
		c.JSON(200, ErrorResp("ERR"+err.Error()))
		return
	}

	var a []structs.Analyze_create
	c.ShouldBind(&a)
	transaksi := c.Param("transaksi")

	if transaksi == "masuk" {
		var ca []database.Analyze_income
		var cc, cx database.Analyze_income
		database.DB.Where("id_nasabah = ?", id).Delete(&cx)
		for _, t := range a {
			cc.Nominal = t.Nominal
			cc.Remarks = t.Remarks
			cc.Id_nasabah = uint(id)
			ca = append(ca, cc)
		}
		err1 := database.DB.CreateInBatches(&ca, 100)
		if err1.Error != nil {
			c.JSON(200, ErrorResp(err1.Error.Error()))
		}

	} else if transaksi == "keluar" {
		var ca []database.Analyze_expense
		var cc, cx database.Analyze_expense
		database.DB.Where("id_nasabah = ?", id).Delete(&cx)
		for _, t := range a {
			cc.Nominal = t.Nominal
			cc.Remarks = t.Remarks
			cc.Id_nasabah = uint(id)
			ca = append(ca, cc)
		}
		/* for _, a2 := range ca {
			//log.Println(a2.Remarks)
		} */
		err1 := database.DB.CreateInBatches(&ca, 100)
		if err1.Error != nil {
			c.JSON(200, ErrorResp(err1.Error.Error()))
		}
		//log.Println(err1.RowsAffected)
	}
	var ak []structs.Analyze_expense
	var am []structs.Analyze_income
	database.DB.Where("id_nasabah = ? AND deleted_at is NULL", id).Find(&ak)
	database.DB.Where("id_nasabah = ? AND deleted_at is NULL", id).Find(&am)
	res := Analyzing(ak, am, id)
	c.JSON(200, SuccessRespData(res))
}

func InitialAnalyze(c *gin.Context) {
	id, err := getId(c)
	if err != nil {
		log.Println("Err : ", err.Error())
	}
	tk, tm := Mutasi2(uint(id))
	hk, hm := GetDuplicateRemarks(tk), GetDuplicateRemarks(tm)
	hk, hm = DeleteBelow3(hk), DeleteBelow3(hm)
	de := database.Analyze_expense{}
	di := database.Analyze_income{}
	database.DB.Unscoped().Where("id_nasabah = ?", id).Delete(&de)
	database.DB.Unscoped().Where("id_nasabah = ?", id).Delete(&di)
	preWrite(hk, id, "keluar")
	preWrite(hm, id, "masuk")
	var ak []structs.Analyze_expense
	var am []structs.Analyze_income
	database.DB.Where("id_nasabah = ? AND deleted_at is NULL", id).Find(&ak)
	database.DB.Where("id_nasabah = ? AND deleted_at is NULL", id).Find(&am)
	res := Analyzing(ak, am, id)
	c.JSON(200, &res)
	return
}

func ApplyInvestment(c *gin.Context) {
	id, err := getId(c)
	if err != nil {
		log.Println("ERR: ", err.Error())
	}
	var p structs.Pengajuan_nasabah
	c.ShouldBind(&p)
	pengajuan := database.Daftar_Pengajuan{Id_nasabah: uint(id), Id_produk_investasi: p.Id_produk, Jumlah_pengajuan: p.Jumlah_investasi, Pengajuan_Status: 1}
	res := database.DB.Create(&pengajuan)
	if res.Error != nil {
		ErrorResp("ERR: " + res.Error.Error())
		return
	}
	SucResp(c, http.StatusOK, "Pengajuan Berhasil", gin.H{"status": "success"})
	return
}
