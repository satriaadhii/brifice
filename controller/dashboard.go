package controller

import (
	"brifice/database"
	"brifice/utils"
	"math"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type Dashboard struct{}
type DashboardResponse struct {
	RequestedTimestamp  time.Time `json:"timestamp"`
	Nama                string    `json:"nama"`
	NamaProdukInvestasi string    `json:"produk_investasi"`
	JumlahPengajuan     float64   `json:"nominal_pengajuan"`
	NamaStatus          string    `json:"status"`
}
type DashboardGraf struct {
	Date   time.Month `json:"bulan"`
	Jumlah uint       `json:"jumlah"`
}

func (d *Dashboard) Dashboard(c *gin.Context) {
	var a []DashboardResponse
	n := database.DB.Debug().Find(&[]database.Nasabah{}).RowsAffected
	p := database.DB.Debug().Find(&[]database.Daftar_Pengajuan{}).RowsAffected
	b := database.DB.Debug().Where("id_produk_investasi = ?", 1).Find(&[]database.Daftar_Pengajuan{}).RowsAffected
	pg := utils.Paginate(c)
	totalData := database.DB.Where("pengajuan_status = ?", 1).Find(&[]database.Daftar_Pengajuan{}).RowsAffected
	queryBuilder := database.DB.Limit(pg.Limit).Offset((pg.Page - 1) * pg.Limit).Order("requested_timestamp desc")

	err := queryBuilder.Debug().Model(&database.Daftar_Pengajuan{}).Select("daftar_pengajuan.requested_timestamp, nasabah.nama, produk_investasi.nama_produk_investasi, daftar_pengajuan.jumlah_pengajuan, status_pengajuan.nama_status").Joins("left join nasabah on nasabah.id = daftar_pengajuan.id_nasabah").Joins("left join produk_investasi on produk_investasi.id = daftar_pengajuan.id_produk_investasi").Joins("left join status_pengajuan on status_pengajuan.id = daftar_pengajuan.pengajuan_status").Where("daftar_pengajuan.pengajuan_status = ?", 1).Scan(&a)
	if err.Error != nil {
		c.JSON(200, gin.H{"status": "gagal", "detail": err.Error.Error()})
		return
	}
	x := gin.H{"pengajuan_baru": &a, "total_pengajuan_baru": totalData, "total_nasabah": n, "total_pengajuan": p, "total_pengajuan_produk": b}
	SucResp(c, 200, x, gin.H{"page": pg.Page, "total_data": totalData, "total_page": math.Ceil(float64(totalData) / float64(pg.Limit))})
	return
}

func (d *Dashboard) PengajuanBaru(c *gin.Context) {
	var a []DashboardResponse
	var err *gorm.DB
	search := c.Query("search")
	pg := utils.Paginate(c)
	totalData := database.DB.Where("pengajuan_status = ?", 1).Find(&[]database.Daftar_Pengajuan{}).RowsAffected
	queryBuilder := database.DB.Limit(pg.Limit).Offset((pg.Page - 1) * pg.Limit).Order("requested_timestamp desc")
	if search == "" {
		err = queryBuilder.Debug().Model(&database.Daftar_Pengajuan{}).Select("daftar_pengajuan.requested_timestamp, nasabah.nama, produk_investasi.nama_produk_investasi, daftar_pengajuan.jumlah_pengajuan, status_pengajuan.nama_status").Joins("left join nasabah on nasabah.id = daftar_pengajuan.id_nasabah").Joins("left join produk_investasi on produk_investasi.id = daftar_pengajuan.id_produk_investasi").Joins("left join status_pengajuan on status_pengajuan.id = daftar_pengajuan.pengajuan_status").Where("daftar_pengajuan.pengajuan_status = ?", 1).Scan(&a)
	} else {
		err = queryBuilder.Debug().Model(&database.Daftar_Pengajuan{}).Select("daftar_pengajuan.requested_timestamp, nasabah.nama, produk_investasi.nama_produk_investasi, daftar_pengajuan.jumlah_pengajuan, status_pengajuan.nama_status").Joins("left join nasabah on nasabah.id = daftar_pengajuan.id_nasabah").Joins("left join produk_investasi on produk_investasi.id = daftar_pengajuan.id_produk_investasi").Joins("left join status_pengajuan on status_pengajuan.id = daftar_pengajuan.pengajuan_status").Where("nasabah.nama ILIKE ? OR produk_investasi.nama_produk_investasi ILIKE ? AND daftar_pengajuan.pengajuan_status = ?", "%"+search+"%", "%"+search+"%", 1).Scan(&a)
	}
	if err.Error != nil {
		c.JSON(200, gin.H{"status": "gagal", "detail": err.Error.Error()})
		return
	}
	res := gin.H{"pengajuan_baru": &a, "total_pengajuan": totalData}
	SucResp(c, 200, res, gin.H{"page": pg.Page, "total_data": totalData, "total_page": math.Ceil(float64(totalData) / float64(pg.Limit))})
	return
}

func (d *Dashboard) JumlahPengajuanByProduk(c *gin.Context) {
	var prid, thn int
	produk, tahun := c.Query("produk"), c.Query("tahun")
	var a []DashboardGraf
	var q, total *gorm.DB
	if tahun == "" {
		thn = time.Now().Year()
	} else {
		thn, _ = strconv.Atoi(tahun)
	}
	if produk == "" {
		prid = 1
	} else {
		prid, _ = strconv.Atoi(produk)
	}

	q = database.DB.Debug().Model(&database.Daftar_Pengajuan{}).Where("id_produk_investasi = ? AND extract(year from requested_timestamp) = ?", prid, thn).Select("extract(month from requested_timestamp) as date, count(*) as jumlah").Group("date").Find(&a)
	if q.Error != nil {
		c.JSON(200, gin.H{"status": "error"})
		return
	}

	total = database.DB.Debug().Where("id_produk_investasi = ?", prid).Find(&[]database.Daftar_Pengajuan{})
	if total.Error != nil {
		c.JSON(200, gin.H{"status": "error"})
		return
	}
	res := gin.H{"graf": &a, "total_pengajuan": &total.RowsAffected}
	SucResp(c, 200, &res, gin.H{"status": "success"})
	return
}

func (d *Dashboard) TotalPengajuan(c *gin.Context) {
	var thn int
	var a []DashboardGraf
	tahun := c.Query("tahun")
	if tahun == "" {
		thn = time.Now().Year()
	} else {
		thn, _ = strconv.Atoi(tahun)
	}
	q := database.DB.Debug().Model(&database.Daftar_Pengajuan{}).Where("extract(year from requested_timestamp) = ?", thn).Select("extract(month from requested_timestamp) as date, count(*) as jumlah").Group("date").Find(&a)
	total := database.DB.Debug().Find(&[]database.Daftar_Pengajuan{}).RowsAffected
	if q.Error != nil {
		c.JSON(200, gin.H{"status": "error"})
		return
	}
	res := gin.H{"graf": &a, "total_pengajuan": &total}
	SucResp(c, 200, &res, gin.H{"status": "success"})
	return
}

func (d *Dashboard) TotalPengguna(c *gin.Context) {
	var a []DashboardGraf
	var thn int
	tahun := c.Query("tahun")
	if tahun == "" {
		thn = time.Now().Year()
	} else {
		thn, _ = strconv.Atoi(tahun)
	}
	q := database.DB.Debug().Model(&database.Nasabah{}).Where("extract(year from created_at) = ?", thn).Select("extract(month from created_at) as date, count(*) as jumlah").Group("date").Find(&a)
	total := database.DB.Debug().Find(&[]database.Nasabah{}).RowsAffected
	if q.Error != nil {
		c.JSON(200, gin.H{"status": "error"})
		return
	}
	res := gin.H{"graf": &a, "total_nasabah": &total}
	SucResp(c, 200, &res, gin.H{"status": "success"})
	return
}
